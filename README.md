# Ghorg docker version

This repository contains tools to create a ghorg docker image.

Ghorg is a tool created by Jay Gabriels original repository is 
located at [github](https://github.com/gabrie30/ghorg).

## Docker image manual creation
If you want to create your own vix image you must type this in a shell:
```
docker build -t ghorg .
```

## Configuration
It's recommended to use a configuration file, you can get an example file from [original repository](https://github.com/gabrie30/ghorg/blob/master/sample-conf.yaml). This configuration file must be renamed to `conf.yaml`.

## Usage

To make easier usage, a run script is provided. This script will pull ghorg from gitlab registry and them run it.

There are several two ways you can run it, with or without params.

1. __Run with default values__
```
#    ./run.sh
```
or
```
#    ./run.sh ghorg
```

2. __Run with custom params or filename__

```
#    ./run.sh ghorg param1 param2 ....
```