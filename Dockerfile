FROM golang:alpine3.14

RUN set -ex; \
    apk update; \
    apk upgrade; \
    apk --no-cache add \
        curl \
        git \
        bash \
    ; \
    # Clean stage
    rm -rf /var/cache/apk/*; \
    # Add default group
    addgroup -g "1000" "group"; \
      # Add default user. In Alpine long-args are not valid, equivalences instead
    adduser \
        # --home "/home/user" \
        -h "/home/user" \
        # --gecos "default user" \
        -g "default user" \
        # --shell "/bin/bash" \
        -s "/bin/bash" \
        # --uid "1000" \
        -u "1000" \
        # --gid "1000" \
        -G "group" \
        # --disabled-password \
        -D \
    "user";

USER 1000:1000

WORKDIR /home/user

RUN go install github.com/gabrie30/ghorg@latest
RUN mkdir -p .config/ghorg repos
RUN curl https://raw.githubusercontent.com/gabrie30/ghorg/master/sample-conf.yaml > .config/ghorg/conf.yaml

CMD [ "ghorg" ]
