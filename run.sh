#!/bin/sh

docker run -it --rm \
    --name ghorg \
    -v $(pwd)/config:/home/user/.config/ghorg \
    -v $(pwd)/repos:/home/user/repos \
    registry.gitlab.com/kalifato/ghorg:latest \
    $@
